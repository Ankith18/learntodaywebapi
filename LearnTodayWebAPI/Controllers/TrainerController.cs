﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LearnTodayWebAPI.Controllers
{
    public class TrainerController : ApiController
    {
        
       [HttpPost]
        public HttpResponseMessage TrainerSignUp([FromBody]Trainer model)
        {
            try
            {
                using (LearnTodayWebAPIDbContext dbContext = new LearnTodayWebAPIDbContext())
                {
                   
                        dbContext.Trainers.Add(model);
                        dbContext.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.Created, model);
                    
                }
            }
           

            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        [HttpPut]
        public HttpResponseMessage UpdatePassword(int id,[FromBody]Trainer model)
        {
            try
            {
                using (LearnTodayWebAPIDbContext dbContext = new LearnTodayWebAPIDbContext())
                {
                    Trainer entity = dbContext.Trainers.FirstOrDefault(x => x.TrainerId == id);
                    if (entity != null)
                    {

                        entity.Password = model.Password;
                        dbContext.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, "Data updated successfully");
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Searched Data not Found");
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
        }
    }
}
