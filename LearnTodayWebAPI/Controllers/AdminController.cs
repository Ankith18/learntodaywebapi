﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LearnTodayWebAPI.Controllers
{
    public class AdminController : ApiController
    {
        public IEnumerable<Course> GetAllCourses()
        {
            using (LearnTodayWebAPIDbContext dbContext = new LearnTodayWebAPIDbContext())
            {
                
                return dbContext.Courses.ToList();
            }
        }
        public HttpResponseMessage GetCourseById(int id)
        {
            
            using (LearnTodayWebAPIDbContext dbContext = new LearnTodayWebAPIDbContext())
            {
                Course course = dbContext.Courses.Where(x => x.CourseId == id).FirstOrDefault();
                if (course == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Searched Data not Found");
                }else
                {
                    return Request.CreateResponse(HttpStatusCode.OK,course);
                }
            }
        }
    }
}
