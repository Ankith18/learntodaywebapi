﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LearnTodayWebAPI.Controllers
{
    public class StudentController : ApiController
    {
        public IEnumerable<Course> GetAllCourses()
        {

            using (LearnTodayWebAPIDbContext dbContext = new LearnTodayWebAPIDbContext())
            {
                var CourseOrder = dbContext.Courses.OrderBy(s => s.Start_Date);
              
                return CourseOrder.ToList();
            }
        }
        public HttpResponseMessage PostStudent([FromBody] Student model)
        {
            try
            {
                using (LearnTodayWebAPIDbContext dbContext = new LearnTodayWebAPIDbContext())
                {
                    dbContext.Students.Add(model);
                    dbContext.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.Created,model);
                    
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        public HttpResponseMessage DeleteStudentEnrollment(int id)
        {
            try
            {
                using (LearnTodayWebAPIDbContext dbContext = new LearnTodayWebAPIDbContext())
                {
                    Student entity = dbContext.Students.FirstOrDefault(x =>x.EnrollmentId == id);
                    if (entity != null)
                    {
                        dbContext.Students.Remove(entity);
                        dbContext.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK);
                      
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No enrollement information found");
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }
        }
    }
}
